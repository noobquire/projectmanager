﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ProjectManager.DTO;

namespace ProjectManager
{
    public static class Api
    {
        private static readonly HttpClient client = new HttpClient();

        public static async Task<IEnumerable<ProjectDTO>> GetProjectsAsync()
        {
            HttpResponseMessage response = await client.GetAsync("https://bsa2019.azurewebsites.net/api/projects");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IEnumerable<ProjectDTO>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<ProjectDTO> GetProjectAsync(int id)
        {
            HttpResponseMessage response = await client.GetAsync($"https://bsa2019.azurewebsites.net/api/projects/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<ProjectDTO>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IEnumerable<TaskDTO>> GetTasksAsync()
        {
            HttpResponseMessage response = await client.GetAsync("https://bsa2019.azurewebsites.net/api/tasks");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IEnumerable<TaskDTO>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<TaskDTO> GetTaskAsync(int id)
        {
            HttpResponseMessage response = await client.GetAsync($"https://bsa2019.azurewebsites.net/api/tasks/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<TaskDTO>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IEnumerable<TeamDTO>> GetTeamsAsync()
        {
            HttpResponseMessage response = await client.GetAsync("https://bsa2019.azurewebsites.net/api/teams");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IEnumerable<TeamDTO>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<TeamDTO> GetTeamAsync(int id)
        {
            HttpResponseMessage response = await client.GetAsync($"https://bsa2019.azurewebsites.net/api/teams/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<TeamDTO>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<IEnumerable<UserDTO>> GetUsersAsync()
        {
            HttpResponseMessage response = await client.GetAsync("https://bsa2019.azurewebsites.net/api/users");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<IEnumerable<UserDTO>>(await response.Content.ReadAsStringAsync());
        }

        public static async Task<UserDTO> GetUserAsync(int id)
        {
            HttpResponseMessage response = await client.GetAsync($"https://bsa2019.azurewebsites.net/api/users/{id}");
            response.EnsureSuccessStatusCode();

            return JsonConvert.DeserializeObject<UserDTO>(await response.Content.ReadAsStringAsync());
        }
    }
}
