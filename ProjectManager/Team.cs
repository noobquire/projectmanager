﻿using ProjectManager.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectManager
{
    public class Team
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }

        public Team(TeamDTO dto)
        {
            this.Name = dto.Name;
            this.CreatedAt = dto.CreatedAt;
        }
    }
}
