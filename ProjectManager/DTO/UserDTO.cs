﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectManager.DTO
{
    [Serializable]
    public class UserDTO
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }
        [JsonProperty(PropertyName = "first_name")]
        public string FirstName { get; set; }
        [JsonProperty(PropertyName = "last_name")]
        public string LastName { get; set; }
        [JsonProperty(PropertyName = "email")]
        public string Email { get; set; }
        [JsonProperty(PropertyName = "birthday")]
        public DateTime Birthday { get; set; }
        [JsonProperty(PropertyName = "registered_at")]
        public DateTime RegisteredAt { get; set; }
        [JsonProperty(PropertyName = "team_id")]
        public int? TeamId { get; set; }
    }
}
