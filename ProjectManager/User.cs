﻿using ProjectManager.DTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectManager
{
    public class User
    {
        public List<Task> Tasks;
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }
        public Team Team { get; set; }

        public User(UserDTO dto)
        {
            this.FirstName = dto.FirstName;
            this.LastName = dto.LastName;
            this.Email = dto.Email;
            this.Birthday = dto.Birthday;
            this.RegisteredAt = dto.RegisteredAt;
            if(dto.TeamId != null)
                this.Team = new Team(Api.GetTeamAsync(dto.TeamId.Value).Result);
        }
    }
}
