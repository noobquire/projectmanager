﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectManager
{
    [Serializable]
    public enum TaskState
    {
        Created = 0,
        Started,
        Finished,
        Cancelled
    }
}
