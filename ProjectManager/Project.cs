﻿using ProjectManager.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ProjectManager
{
    public class Project
    {
        public readonly List<Task> Tasks;
        public User Author { get; set; }
        public Team Team { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }

        public Project(ProjectDTO dto)
        {
            this.Tasks = Api.GetTasksAsync().Result.Where(t => t.ProjectId == dto.Id).Select(t => new Task(t, false) { Project = this }).ToList();
            this.Name = dto.Name;
            var author = Api.GetUserAsync(dto.AuthorId).Result;
            this.Author = new User(author);
            this.Team = new Team(Api.GetTeamAsync(dto.TeamId).Result);
            this.Name = dto.Name;
            this.Description = dto.Description;
            this.CreatedAt = dto.CreatedAt;
            this.Deadline = dto.Deadline;
        }
    }

}
